package com.example.hejuso.actividadpreferences;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class ResultadosActivity extends AppCompatActivity {
    public static final String PREFS = "Preferencias";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultados);

        SharedPreferences mySharedPreferences = getSharedPreferences(PREFS, ResultadosActivity.MODE_PRIVATE);

        final TextView nombre = findViewById(R.id.nombre);
        final TextView dni = findViewById(R.id.dni);
        final TextView fecha = findViewById(R.id.fecha);
        final TextView sexo = findViewById(R.id.sexo);

        String nombre_str = mySharedPreferences.getString("nombre","");
        String dni_str = mySharedPreferences.getString("dni","");
        String fecha_str = mySharedPreferences.getString("fecha","");
        String sexo_str = mySharedPreferences.getString("sexo","");

        nombre.setText(nombre_str);
        dni.setText(dni_str);
        fecha.setText(fecha_str);
        sexo.setText(sexo_str);
    }
}
