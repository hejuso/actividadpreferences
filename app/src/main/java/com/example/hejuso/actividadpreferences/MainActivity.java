package com.example.hejuso.actividadpreferences;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    public static final String PREFS = "Preferencias";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Button button = findViewById(R.id.guardar);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                // Creamos o recuperamos el objeto de preferencias compartidas
                SharedPreferences mySharedPreferences = getSharedPreferences(PREFS, MainActivity.MODE_PRIVATE);

                // Obtenemos un editor para modificar las preferencias
                SharedPreferences.Editor editor = mySharedPreferences.edit();

                // Guardamos los valores
                final EditText nombre = findViewById(R.id.nombre);
                final EditText dni = findViewById(R.id.dni);
                final EditText fecha = findViewById(R.id.fecha);
                final RadioButton hombre = findViewById(R.id.hombre);
                final RadioButton mujer = findViewById(R.id.mujer);

                //Guardamos el nombre
                editor.putString("nombre",nombre.getText().toString());
                //Guardamos el dni
                editor.putString("dni",dni.getText().toString());
                //Guardamos la fecha
                editor.putString("fecha",fecha.getText().toString());

                //Guardamos el sexo
                if (hombre.isChecked()){
                 editor.putString("sexo",hombre.getHint().toString());
                }else if (mujer.isChecked()){
                 editor.putString("sexo",mujer.getHint().toString());
                }

                // Guardamos los cambios
                editor.commit();

                Intent mainIntent = new Intent(getApplicationContext(),
                        ResultadosActivity.class);
                startActivity(mainIntent);

            }
        });

    }
}
